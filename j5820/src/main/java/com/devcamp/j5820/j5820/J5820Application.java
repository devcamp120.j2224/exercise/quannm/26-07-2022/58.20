package com.devcamp.j5820.j5820;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J5820Application {

	public static void main(String[] args) {
		SpringApplication.run(J5820Application.class, args);
	}

}
