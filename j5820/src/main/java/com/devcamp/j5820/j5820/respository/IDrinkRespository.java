package com.devcamp.j5820.j5820.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j5820.j5820.model.CDrink;

public interface IDrinkRespository extends JpaRepository<CDrink, Long> {
    
}
