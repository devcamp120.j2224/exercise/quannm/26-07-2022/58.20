package com.devcamp.j5820.j5820.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j5820.j5820.model.CDrink;
import com.devcamp.j5820.j5820.respository.IDrinkRespository;


@CrossOrigin
@RestController
@RequestMapping("/")
public class CDrinkController {
    @Autowired
    IDrinkRespository pCustomerRespository;

    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllCDrinks() {
        try {
            List<CDrink> list = new ArrayList<CDrink>();

            pCustomerRespository.findAll().forEach(list::add);

            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
